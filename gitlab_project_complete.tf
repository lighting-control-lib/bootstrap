resource "gitlab_project" "lighting-control-complete" {
  name             = "Lighting-Control"
  path             = "lighting-control-complete"
  description      = "Light Show UI Runner"
  namespace_id     = gitlab_group.lighting-control.id
  avatar           = "logos/lighting-control.png"
  avatar_hash      = filesha256("logos/lighting-control.png")
  topics           = ["lighting-control", "ui", "json", "yaml"]
  visibility_level = "public"
  default_branch   = "main"

  analytics_access_level          = "disabled"
  container_registry_access_level = "disabled"
  environments_access_level       = "disabled"
  feature_flags_access_level      = "disabled"
  infrastructure_access_level     = "private"
  issues_enabled                  = true
  lfs_enabled                     = false
  merge_method                    = "ff"
  merge_trains_enabled            = false
  monitor_access_level            = "disabled"
  packages_enabled                = true
  pages_access_level              = "disabled"
  releases_access_level           = "enabled"
  snippets_enabled                = false
  squash_option                   = "always"
  wiki_enabled                    = false
}

resource "gitlab_project_badge" "lighting-control-complete-pipeline" {
  project   = gitlab_project.lighting-control-complete.id
  link_url  = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name      = "Pipeline"
}

resource "gitlab_project_badge" "lighting-control-complete-license" {
  project   = gitlab_project.lighting-control-complete.id
  link_url  = "https://gitlab.com/%%{project_path}/-/blob/%%{default_branch}/LICENSE.txt"
  image_url = "https://img.shields.io/badge/License-Apache-282661&logo=apache"
  name      = "Apache License"
}

resource "gitlab_project_variable" "lighting-control-complete-deploy-username" {
  project = gitlab_project.lighting-control-complete.id
  key     = "DEPLOY_USERNAME"
  value   = gitlab_deploy_token.lightingcontrol.username
}

resource "gitlab_project_variable" "lighting-control-complete-deploy-key" {
  project = gitlab_project.lighting-control-complete.id
  key     = "DEPLOY_KEY"
  value   = gitlab_deploy_token.lightingcontrol.token
}

resource "gitlab_branch_protection" "lighting-control-complete-main" {
  project                = gitlab_project.lighting-control-complete.id
  branch                 = gitlab_project.lighting-control-complete.default_branch
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false
}
