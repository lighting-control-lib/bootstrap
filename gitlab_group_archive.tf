# The Experiment sub group
resource "gitlab_group" "archive" {
  name        = "Archive"
  path        = "archive"
  description = "Archive Lighting-Control Projects"
  avatar      = "logos/web.png"
  avatar_hash = filesha256("logos/web.png")
  parent_id   = gitlab_group.lighting-control.id
  depends_on  = [gitlab_group.lighting-control]

  auto_devops_enabled    = false
  lfs_enabled            = false
  request_access_enabled = false
  visibility_level       = "public"
}
