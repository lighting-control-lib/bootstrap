# PTZ Camera control lighting-control Library
resource "gitlab_project" "lighting-control-runner" {
  name             = "Lighting-Control Runner"
  path             = "runner"
  description      = "Attempt to port the project to Rust"
  namespace_id     = gitlab_group.archive.id
  avatar           = "logos/runner.png"
  avatar_hash      = filesha256("logos/runner.png")
  topics           = ["rust", "lighting", "stage-lighting"]
  visibility_level = "public"
  depends_on       = [gitlab_group.experiments]
  default_branch   = "main"

  archived                        = true
  analytics_access_level          = "disabled"
  container_registry_access_level = "disabled"
  environments_access_level       = "disabled"
  feature_flags_access_level      = "disabled"
  infrastructure_access_level     = "disabled"
  issues_enabled                  = true
  lfs_enabled                     = false
  merge_method                    = "ff"
  merge_trains_enabled            = false
  monitor_access_level            = "disabled"
  packages_enabled                = false
  pages_access_level              = "disabled"
  releases_access_level           = "enabled"
  snippets_enabled                = false
  squash_option                   = "always"
  wiki_enabled                    = false
}

resource "gitlab_project_badge" "lighting-control-runner-pipeline" {
  project    = gitlab_project.lighting-control-runner.id
  link_url   = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url  = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name       = "Pipeline"
  depends_on = [gitlab_branch_protection.lighting-control-runner-main]
}

resource "gitlab_project_badge" "lighting-control-runner-license" {
  project    = gitlab_project.lighting-control-runner.id
  link_url   = "https://gitlab.com/%%{project_path}/-/blob/%%{default_branch}/LICENSE.txt"
  image_url  = "https://img.shields.io/badge/License-Apache-282661&logo=apache"
  name       = "Apache License"
  depends_on = [gitlab_branch_protection.lighting-control-runner-main]
}

resource "gitlab_branch_protection" "lighting-control-runner-main" {
  project                = gitlab_project.lighting-control-runner.id
  branch                 = gitlab_project.lighting-control-runner.default_branch
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false
  depends_on             = [gitlab_branch_protection.lighting-control-runner-main]
}

# resource "gitlab_project_variable" "lighting-control-runner-sonarhost" {
#   project    = gitlab_project.lighting-control-runner.id
#   key        = "SONAR_HOST_URL"
#   value      = var.sonarcloud_host_url
#   depends_on = [gitlab_project.lighting-control-runner]
#   protected  = false
#   masked     = false
#   raw        = true
# }
#
# resource "gitlab_project_variable" "lighting-control-runner-sonartoken" {
#   project    = gitlab_project.lighting-control-runner.id
#   key        = "SONAR_TOKEN"
#   value      = var.sonarcloud_token_runner
#   depends_on = [gitlab_project.lighting-control-runner]
#   protected  = false
#   masked     = true
#   raw        = true
# }
