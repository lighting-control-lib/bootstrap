# Main lighting-control Library
resource "gitlab_project" "lighting-control-library" {
  name             = "Lighting-Control Library"
  path             = "lighting-control"
  description      = "Library to run light shows"
  namespace_id     = gitlab_group.archive.id
  avatar           = "logos/lighting-control.png"
  avatar_hash      = filesha256("logos/lighting-control.png")
  topics           = ["artnet", "dmx", "java-library", "library", "light", "lighting", "lighting-control", "lightshow"]
  visibility_level = "public"
  default_branch   = "main"
  archived         = true

  analytics_access_level          = "disabled"
  container_registry_access_level = "disabled"
  environments_access_level       = "disabled"
  feature_flags_access_level      = "disabled"
  infrastructure_access_level     = "private"
  issues_enabled                  = true
  lfs_enabled                     = false
  merge_method                    = "ff"
  merge_trains_enabled            = false
  monitor_access_level            = "disabled"
  packages_enabled                = true
  pages_access_level              = "disabled"
  releases_access_level           = "enabled"
  snippets_enabled                = false
  squash_option                   = "always"
  wiki_enabled                    = false
}

resource "gitlab_project_badge" "lighting-control-library-pipeline" {
  project   = gitlab_project.lighting-control-library.id
  link_url  = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name      = "Pipeline"
}

resource "gitlab_project_badge" "lighting-control-library-release" {
  project   = gitlab_project.lighting-control-library.id
  link_url  = "https://gitlab.com/%%{project_path}/-/releases"
  image_url = "https://gitlab.com/%%{project_path}/-/badges/release.svg"
  name      = "Pipeline"
}

resource "gitlab_project_badge" "lighting-control-library-license" {
  project   = gitlab_project.lighting-control-library.id
  link_url  = "https://gitlab.com/%%{project_path}/-/blob/%%{default_branch}/LICENSE.txt"
  image_url = "https://img.shields.io/badge/License-Apache-282661&logo=apache"
  name      = "Apache License"
}

# resource "gitlab_project_badge" "lighting-control-library-maintainability" {
#   project   = gitlab_project.lighting-control-library.id
#   link_url  = "https://sonarcloud.io/summary/new_code?id=lightingcontrol_lighting-control"
#   image_url = "https://sonarcloud.io/api/project_badges/measure?project=lightingcontrol_lighting-control&metric=sqale_rating"
#   name      = "Maintainability Rating"
# }

resource "gitlab_branch_protection" "lighting-control-library-main" {
  project                = gitlab_project.lighting-control-library.id
  branch                 = gitlab_project.lighting-control-library.default_branch
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false
}

# resource "gitlab_project_variable" "lighting-control-library-sonarhost" {
#   project   = gitlab_project.lighting-control-library.id
#   key       = "SONAR_HOST_URL"
#   value     = var.sonarcloud_host_url
#   protected = false
#   masked    = false
#   raw       = true
# }
# 
# resource "gitlab_project_variable" "lighting-control-library-sonartoken" {
#   project   = gitlab_project.lighting-control-library.id
#   key       = "SONAR_TOKEN"
#   value     = var.sonarcloud_token_lib
#   protected = true
#   masked    = true
#   raw       = true
# }
