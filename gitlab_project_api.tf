# Experimental lighting-control websocket API
resource "gitlab_project" "lighting-control-api" {
  name             = "Lighting-Control API"
  path             = "lighting-control-api"
  description      = "Experimental Websocket API"
  namespace_id     = gitlab_group.experiments.id
  avatar           = "logos/lighting-control.png"
  avatar_hash      = filesha256("logos/lighting-control.png")
  topics           = ["lighting-control", "api", "websocket"]
  visibility_level = "public"
  default_branch   = "main"

  analytics_access_level          = "disabled"
  container_registry_access_level = "enabled"
  environments_access_level       = "disabled"
  feature_flags_access_level      = "disabled"
  infrastructure_access_level     = "disabled"
  issues_enabled                  = true
  lfs_enabled                     = false
  merge_method                    = "ff"
  merge_trains_enabled            = false
  monitor_access_level            = "disabled"
  packages_enabled                = false
  pages_access_level              = "disabled"
  releases_access_level           = "disabled"
  snippets_enabled                = false
  squash_option                   = "always"
  wiki_enabled                    = false
}

resource "gitlab_project_variable" "lighting-control-api-sonarhost" {
  project   = gitlab_project.lighting-control-api.id
  key       = "SONAR_HOST_URL"
  value     = var.sonarcloud_host_url
  protected = false
  masked    = false
  raw       = true
}

resource "gitlab_project_variable" "lighting-control-api-sonartoken" {
  project   = gitlab_project.lighting-control-api.id
  key       = "SONAR_TOKEN"
  value     = var.sonarcloud_token_api
  protected = false
  masked    = true
  raw       = true
}
