variable "gitlab_token" {
  type        = string
  description = "Access token to gitlab."
  sensitive   = true
}

variable "sonarcloud_host_url" {
  type        = string
  description = "URL for sonarcloud analysis"
  sensitive   = true
  default     = "https://sonarcloud.io"
}

variable "sonarcloud_token_api" {
  type        = string
  description = "Access token to sonarcloud for api project"
  sensitive   = true
}

variable "sonarcloud_token_configuration" {
  type        = string
  description = "Access token to sonarcloud for configuration project"
  sensitive   = true
}

variable "sonarcloud_token_console" {
  type        = string
  description = "Access token to sonarcloud for console project"
  sensitive   = true
}

variable "sonarcloud_token_lib" {
  type        = string
  description = "Access token to sonarcloud for library project"
  sensitive   = true
}

variable "sonarcloud_token_visca" {
  type        = string
  description = "Access token to sonarcloud for visca project"
  sensitive   = true
}
