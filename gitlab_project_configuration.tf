# Configuration project defining the json/yaml configuration
resource "gitlab_project" "lighting-control-configuration" {
  name             = "Lighting-Control Configuration"
  path             = "lighting-control-configuration"
  description      = "Configuration of light shows"
  namespace_id     = gitlab_group.archive.id
  avatar           = "logos/lighting-control.png"
  avatar_hash      = filesha256("logos/lighting-control.png")
  topics           = ["lighting-control", "configuration", "json", "yaml"]
  visibility_level = "public"
  default_branch   = "main"
  archived         = true

  analytics_access_level          = "disabled"
  container_registry_access_level = "disabled"
  environments_access_level       = "disabled"
  feature_flags_access_level      = "disabled"
  infrastructure_access_level     = "private"
  issues_enabled                  = false
  lfs_enabled                     = false
  merge_method                    = "ff"
  merge_trains_enabled            = false
  monitor_access_level            = "disabled"
  packages_enabled                = true
  pages_access_level              = "disabled"
  releases_access_level           = "enabled"
  snippets_enabled                = false
  squash_option                   = "always"
  wiki_enabled                    = false
}

resource "gitlab_project_badge" "lighting-control-configuration-pipeline" {
  project   = gitlab_project.lighting-control-configuration.id
  link_url  = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name      = "Pipeline"
}

resource "gitlab_project_badge" "lighting-control-configuration-license" {
  project   = gitlab_project.lighting-control-configuration.id
  link_url  = "https://gitlab.com/%%{project_path}/-/blob/%%{default_branch}/LICENSE.txt"
  image_url = "https://img.shields.io/badge/License-Apache-282661&logo=apache"
  name      = "Apache License"
}

# resource "gitlab_project_badge" "lighting-control-configuration-maintainability" {
#   project   = gitlab_project.lighting-control-configuration.id
#   link_url  = "https://sonarcloud.io/summary/new_code?id=lightingcontrol_lighting-control-configuration"
#   image_url = "https://sonarcloud.io/api/project_badges/measure?project=lightingcontrol_lighting-control-configuration&metric=sqale_rating"
#   name      = "Maintainability Rating"
# }
# 
# resource "gitlab_project_variable" "lighting-control-configuration-sonarhost" {
#   project   = gitlab_project.lighting-control-configuration.id
#   key       = "SONAR_HOST_URL"
#   value     = var.sonarcloud_host_url
#   protected = false
#   masked    = false
#   raw       = true
# }
# 
# resource "gitlab_project_variable" "lighting-control-configuration-sonartoken" {
#   project   = gitlab_project.lighting-control-configuration.id
#   key       = "SONAR_TOKEN"
#   value     = var.sonarcloud_token_configuration
#   protected = true
#   masked    = true
#   raw       = true
# }
# 
# resource "gitlab_project_variable" "lghting-control-configuration-deploy-username" {
#   project = gitlab_project.lighting-control-configuration.id
#   key     = "DEPLOY_USERNAME"
#   value   = gitlab_deploy_token.lightingcontrol.username
# }
# 
# resource "gitlab_project_variable" "lghting-control-configuration-deploy-key" {
#   project = gitlab_project.lighting-control-configuration.id
#   key     = "DEPLOY_KEY"
#   value   = gitlab_deploy_token.lightingcontrol.token
# }

resource "gitlab_branch_protection" "lighting-control-configuration-main" {
  project                = gitlab_project.lighting-control-configuration.id
  branch                 = gitlab_project.lighting-control-configuration.default_branch
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false
}
