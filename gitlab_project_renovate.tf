# Package Repository project to setup the project resources
resource "gitlab_project" "lighting-control-renovate" {
  name             = "Renovate"
  path             = "renovate"
  description      = "Update Project dependencies for lighting-control"
  avatar           = "logos/renovate.png"
  avatar_hash      = filesha256("logos/renovate.png")
  namespace_id     = gitlab_group.lighting-control.id
  topics           = ["dependency", "renovate"]
  visibility_level = "public"
  default_branch   = "main"

  analytics_access_level               = "disabled"
  auto_devops_enabled                  = false
  ci_forward_deployment_enabled        = false
  container_registry_access_level      = "disabled"
  environments_access_level            = "disabled"
  feature_flags_access_level           = "disabled"
  forking_access_level                 = "disabled"
  infrastructure_access_level          = "disabled"
  issues_enabled                       = false
  lfs_enabled                          = false
  merge_method                         = "ff"
  merge_pipelines_enabled              = false
  merge_requests_enabled               = false
  merge_trains_enabled                 = false
  monitor_access_level                 = "disabled"
  packages_enabled                     = false
  pages_access_level                   = "disabled"
  printing_merge_request_link_enabled  = false
  releases_access_level                = "disabled"
  request_access_enabled               = false
  requirements_access_level            = "disabled"
  security_and_compliance_access_level = "disabled"
  shared_runners_enabled               = false
  snippets_enabled                     = false
  wiki_enabled                         = false
}

resource "gitlab_project_badge" "lighting-control-renovate-pipeline" {
  project   = gitlab_project.lighting-control-renovate.id
  link_url  = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name      = "Pipeline"
}

resource "gitlab_project_badge" "lighting-control-renovate-renovate" {
  project   = gitlab_project.lighting-control-renovate.id
  link_url  = "https://gitlab.com/%%{project_path}"
  image_url = "https://img.shields.io/badge/renovate-latest-357e9e?logo=renovate"
  name      = "Renovate"
}

resource "gitlab_pipeline_schedule" "renovate" {
  project     = gitlab_project.lighting-control-renovate.id
  description = "Check for updates"
  ref         = "refs/heads/main"
  cron        = "4 5 * * 5"
}

resource "gitlab_pipeline_schedule_variable" "renovate-extra-flags" {
  project              = gitlab_pipeline_schedule.renovate.project
  pipeline_schedule_id = gitlab_pipeline_schedule.renovate.pipeline_schedule_id
  key                  = "RENOVATE_EXTRA_FLAGS"

  value = join(" ", [
    # gitlab_project.lighting-control-api.path_with_namespace,
    gitlab_project.lighting-control-bootstrap.path_with_namespace,
    # gitlab_project.lighting-control-configuration.path_with_namespace,
    gitlab_project.lighting-control-complete.path_with_namespace,
    # gitlab_project.lighting-control-library.path_with_namespace,
    # gitlab_project.lighting-control-visca.path_with_namespace,
    # gitlab_project.lighting-control-runner.path_with_namespace,
  ])
}
