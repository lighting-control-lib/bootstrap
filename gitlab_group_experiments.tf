# The Experiment sub group
resource "gitlab_group" "experiments" {
  name        = "Experiments"
  path        = "experiments"
  description = "Lighting-Control Experiment Projects"
  avatar      = "logos/experiment.png"
  avatar_hash = filesha256("logos/experiment.png")
  parent_id   = gitlab_group.lighting-control.id
  depends_on  = [gitlab_group.lighting-control]

  auto_devops_enabled    = false
  lfs_enabled            = false
  request_access_enabled = false
  visibility_level       = "public"
}
