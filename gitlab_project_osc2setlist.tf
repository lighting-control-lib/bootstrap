# PTZ Camera control lighting-control Library
resource "gitlab_project" "osc2setlist" {
  name             = "Open Sond Control Setlist"
  path             = "osc2setlist"
  description      = "Experiment to centralize a setlist via OSC."
  namespace_id     = gitlab_group.experiments.id
  avatar           = "logos/arduino.png"
  avatar_hash      = filesha256("logos/arduino.png")
  topics           = ["osc", "experiment", "arduino", "Setlist"]
  visibility_level = "public"
  depends_on       = [gitlab_group.experiments]
  default_branch   = "main"

  analytics_access_level          = "disabled"
  container_registry_access_level = "disabled"
  environments_access_level       = "disabled"
  feature_flags_access_level      = "disabled"
  infrastructure_access_level     = "disabled"
  issues_enabled                  = true
  lfs_enabled                     = false
  merge_method                    = "ff"
  merge_trains_enabled            = false
  monitor_access_level            = "disabled"
  packages_enabled                = false
  pages_access_level              = "disabled"
  releases_access_level           = "enabled"
  snippets_enabled                = false
  squash_option                   = "always"
  wiki_enabled                    = false
}

resource "gitlab_project_badge" "osc2setlist-pipeline" {
  project    = gitlab_project.osc2setlist.id
  link_url   = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url  = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name       = "Pipeline"
  depends_on = [gitlab_project.osc2setlist]
}

resource "gitlab_project_badge" "osc2setlist-license" {
  project    = gitlab_project.osc2setlist.id
  link_url   = "https://gitlab.com/%%{project_path}/-/blob/%%{default_branch}/LICENSE.txt"
  image_url  = "https://img.shields.io/badge/License-Apache-282661&logo=apache"
  name       = "Apache License"
  depends_on = [gitlab_project.osc2setlist]
}

resource "gitlab_branch_protection" "osc2setlist" {
  project                = gitlab_project.osc2setlist.id
  branch                 = gitlab_project.osc2setlist.default_branch
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false
  depends_on             = [gitlab_branch_protection.osc2setlist]
}
