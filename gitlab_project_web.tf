# Webpage for lighting-control
resource "gitlab_project" "lightingcontrol-gitlab-io" {
  name             = "Lighting-Control Web"
  path             = "lightingcontrol.gitlab.io"
  description      = "Sources for https://lightingcontrol.gitlab.io/"
  namespace_id     = gitlab_group.archive.id
  avatar           = "logos/web.png"
  avatar_hash      = filesha256("logos/web.png")
  topics           = ["lighting-control", "web"]
  visibility_level = "public"
  default_branch   = "main"

  archived                             = true
  analytics_access_level               = "disabled"
  container_registry_access_level      = "disabled"
  environments_access_level            = "enabled"
  feature_flags_access_level           = "disabled"
  infrastructure_access_level          = "disabled"
  issues_enabled                       = true
  lfs_enabled                          = false
  merge_method                         = "ff"
  merge_trains_enabled                 = false
  monitor_access_level                 = "disabled"
  packages_enabled                     = false
  pages_access_level                   = "disabled"
  releases_access_level                = "disabled"
  security_and_compliance_access_level = "disabled"
  snippets_enabled                     = false
  squash_option                        = "always"
  wiki_enabled                         = false
}

resource "gitlab_project_environment" "lightingcontrol-gitlab-io" {
  project      = gitlab_project.lightingcontrol-gitlab-io.id
  name         = "page"
  external_url = "https://lightingcontrol.gitlab.io/"
}

resource "gitlab_project_badge" "lightingcontrol-gitlab-io-angular" {
  project   = gitlab_project.lightingcontrol-gitlab-io.id
  link_url  = "https://gohugo.io/"
  image_url = "https://img.shields.io/badge/angular-16-dd0031?logo=angular"
  name      = "Angular"
}

# resource "gitlab_project_variable" "lighting-control-web-sonarhost" {
#   project   = gitlab_project.lightingcontrol-gitlab-io.id
#   key       = "SONAR_HOST_URL"
#   value     = var.sonarcloud_host_url
#   protected = false
#   masked    = false
#   raw       = true
# }
# 
# resource "gitlab_project_variable" "lighting-control-web-sonartoken" {
#   project   = gitlab_project.lightingcontrol-gitlab-io.id
#   key       = "SONAR_TOKEN"
#   value     = var.sonarcloud_token_web
#   protected = false
#   masked    = true
#   raw       = true
# }

resource "gitlab_project_label" "angular-component" {
  project     = gitlab_project.lightingcontrol-gitlab-io.id
  name        = "angular:component"
  description = "Work on Components"
  color       = "#ffa500"
}

resource "gitlab_project_label" "angular-service" {
  project     = gitlab_project.lightingcontrol-gitlab-io.id
  name        = "angular:service"
  description = "Work on Services"
  color       = "#ffa500"
}
