# Bootstrap project to setup the project resources
resource "gitlab_project" "lighting-control-bootstrap" {
  name             = "Bootstrap"
  path             = "bootstrap"
  description      = "Bootstrap Project for lighting-control"
  namespace_id     = gitlab_group.lighting-control.id
  avatar           = "logos/bootstrap.png"
  avatar_hash      = filesha256("logos/bootstrap.png")
  topics           = ["gitlab", "bootstrap", "terraform"]
  visibility_level = "public"
  default_branch   = "main"

  analytics_access_level      = "disabled"
  environments_access_level   = "enabled"
  feature_flags_access_level  = "disabled"
  infrastructure_access_level = "private"
  issues_enabled              = false
  lfs_enabled                 = false
  merge_method                = "ff"
  merge_trains_enabled        = false
  monitor_access_level        = "disabled"
  packages_enabled            = false
  pages_access_level          = "disabled"
  releases_access_level       = "disabled"
  snippets_enabled            = false
  squash_option               = "always"
  wiki_enabled                = false
}

resource "gitlab_project_badge" "lighting-control-bootstrap-pipeline" {
  project   = gitlab_project.lighting-control-bootstrap.id
  link_url  = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name      = "Pipeline"
}

resource "gitlab_project_badge" "lighting-control-bootstrap-terraform" {
  project   = gitlab_project.lighting-control-bootstrap.id
  link_url  = "https://gitlab.com/%%{project_path}"
  image_url = "https://img.shields.io/badge/terraform-1.5-7B42BC?logo=terraform"
  name      = "Terraform"
}

resource "gitlab_project_badge" "lighting-control-bootstrap-license" {
  project   = gitlab_project.lighting-control-bootstrap.id
  link_url  = "https://gitlab.com/%%{project_path}/-/blob/%%{default_branch}/LICENSE.txt"
  image_url = "https://img.shields.io/badge/License-Apache-282661?logo=apache"
  name      = "Apache License"
}

resource "gitlab_branch_protection" "lighting-control-bootstrap-main" {
  project                = gitlab_project.lighting-control-bootstrap.id
  branch                 = gitlab_project.lighting-control-bootstrap.default_branch
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false
}
