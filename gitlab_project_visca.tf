# PTZ Camera control lighting-control Library
resource "gitlab_project" "lighting-control-visca" {
  name             = "Lighting-Control Visca Library"
  path             = "visca"
  description      = "Library to control PTZ cameras"
  namespace_id     = gitlab_group.experiments.id
  avatar           = "logos/camera.png"
  avatar_hash      = filesha256("logos/camera.png")
  topics           = ["camera", "ptz", "java-library", "library", "java", "visca"]
  visibility_level = "public"
  depends_on       = [gitlab_group.experiments]
  default_branch   = "main"

  analytics_access_level          = "disabled"
  container_registry_access_level = "disabled"
  environments_access_level       = "disabled"
  feature_flags_access_level      = "disabled"
  infrastructure_access_level     = "disabled"
  issues_enabled                  = true
  lfs_enabled                     = false
  merge_method                    = "ff"
  merge_trains_enabled            = false
  monitor_access_level            = "disabled"
  packages_enabled                = false
  pages_access_level              = "disabled"
  releases_access_level           = "enabled"
  snippets_enabled                = false
  squash_option                   = "always"
  wiki_enabled                    = false
}

resource "gitlab_project_badge" "lighting-control-visca-pipeline" {
  project    = gitlab_project.lighting-control-visca.id
  link_url   = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url  = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name       = "Pipeline"
  depends_on = [gitlab_branch_protection.lighting-control-visca-main]
}

resource "gitlab_project_badge" "lighting-control-visca-license" {
  project    = gitlab_project.lighting-control-visca.id
  link_url   = "https://gitlab.com/%%{project_path}/-/blob/%%{default_branch}/LICENSE.txt"
  image_url  = "https://img.shields.io/badge/License-Apache-282661&logo=apache"
  name       = "Apache License"
  depends_on = [gitlab_branch_protection.lighting-control-visca-main]
}

resource "gitlab_branch_protection" "lighting-control-visca-main" {
  project                = gitlab_project.lighting-control-visca.id
  branch                 = gitlab_project.lighting-control-visca.default_branch
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false
  depends_on             = [gitlab_branch_protection.lighting-control-visca-main]
}

resource "gitlab_project_variable" "lighting-control-visca-sonarhost" {
  project   = gitlab_project.lighting-control-visca.id
  key       = "SONAR_HOST_URL"
  value     = var.sonarcloud_host_url
  protected = false
  masked    = false
  raw       = true
}

resource "gitlab_project_variable" "lighting-control-visca-sonartoken" {
  project   = gitlab_project.lighting-control-visca.id
  key       = "SONAR_TOKEN"
  value     = var.sonarcloud_token_visca
  protected = false
  masked    = true
  raw       = true
}
