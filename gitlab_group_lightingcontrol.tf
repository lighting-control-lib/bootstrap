# The project group
resource "gitlab_group" "lighting-control" {
  name        = "Lighting-Control"
  path        = "lightingcontrol"
  description = "Lighting-Control Project"
  avatar      = "logos/lighting-control.png"
  avatar_hash = filesha256("logos/lighting-control.png")

  lfs_enabled                       = false
  require_two_factor_authentication = true
  visibility_level                  = "public"
}

resource "gitlab_group_membership" "rynr" {
  group_id     = gitlab_group.lighting-control.id
  user_id      = data.gitlab_user.rynr.id
  access_level = "owner"
}

resource "gitlab_group_label" "lighting-control-bug" {
  group       = gitlab_group.lighting-control.id
  name        = "bug"
  description = "Marks a software bug"
  color       = "#dd2222"
}

resource "gitlab_group_label" "lighting-control-documentation" {
  group       = gitlab_group.lighting-control.id
  name        = "documentation"
  description = "Marks a documentation task"
  color       = "#2222dd"
}

resource "gitlab_group_label" "lighting-control-on-hold" {
  group       = gitlab_group.lighting-control.id
  name        = "on-hold"
  description = "Marks a merge request which cannot be merged now"
  color       = "#eedd22"
}

resource "time_rotating" "lightingcontrol-deploy-key" {
  rotation_months = 1
}

resource "gitlab_deploy_token" "lightingcontrol" {
  name       = "maven repository"
  group      = gitlab_group.lighting-control.id
  scopes     = ["read_package_registry", "write_package_registry"]
  expires_at = timeadd(time_rotating.lightingcontrol-deploy-key.rfc3339, format("%gh", 24 * 30 * 2)) // ~2 months
}
