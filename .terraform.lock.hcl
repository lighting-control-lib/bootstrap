# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version = "17.9.0"
  hashes = [
    "h1:+CJrTH2UaLngmMx6LRirUK1TO+xwrhvPCC4rlJI0lj4=",
    "h1:2TL3L7L6efP/X2ReYOmBZL65llkbze1KjTmFRVZR3fI=",
    "h1:C7Y/oMJw3+70ywNYmMttabv7HlTxMPJysK1egbSZZpI=",
    "h1:LPf+sRVSDW9U95KyIM6R3e9fIo0KtFM9hvJkbe/Cuhg=",
    "h1:R1QhLiFsckFttQDAcLDqpvFAnu3nOLFXW4QtZ8lhXQ0=",
    "h1:aRikYpBcQKe7ZVq263R3lwo4DawGw1rMJnxnUzvDGUA=",
    "h1:fOLkxMi69XiLgAEBBqNvf7RwVAgyHjDQpgBg1msmYcY=",
    "h1:mVYfF+4UtQ5FYZR3lcfZZC9cCpLEtb4Ajlj0QULDEXM=",
    "h1:o/F981IxM5BcY77xSKshC6kkYxYAEQ0HSJzUrC72zsM=",
    "h1:raR4dKK3hbPFeqf5+22ZQj0ukZd1TyEtmwhy7NoYRgY=",
    "h1:ulm4DneGmCySerJgZfierGC30/3aNflKeQ1iHb5Mj08=",
    "h1:vFFMVzgLE/YY86mSxqrMBbtiIfP3Jtx4aQLQGtNFntA=",
    "h1:wBIzMhLhAesjT1xhxbhh9f67BrwBO6LcOhj1WS5DlxQ=",
    "h1:xTTLTGbdmDSKZRajfdI0YT8uR5M6LwWdaovKi1xR07U=",
    "zh:08b4a0f843f592f338e403293063150a47e6daa682d38541b6418591a71aa6b2",
    "zh:0c174c227b8721b08908e285db2cf791e848ceb55759cca67aaee19d9df65d6f",
    "zh:244f74e4ab3688aee5d2b027cfb9c6a368a9195c0aa38c54c3fbc19d2b3c12f9",
    "zh:2891429d88037f44095c499a3d1c9a6423b3bd735786010c15b1d9c0bd2cc97a",
    "zh:30b6d94925694f9e01f2adeb41544a9eaaac96872fb693b0594a8765f7b2e609",
    "zh:40443f62958d4b4038da6abd1aabd699eda25bc12e8829f91585a25a0e357bae",
    "zh:426d3927e7453532b5a1ce2e9a1c16bff4e032588a6f89b0647aceb3dd69739b",
    "zh:484dbc9e1003d9ff910fa051ac78e69be147e4b7b6d47d11d66bfcb1d10cb5c2",
    "zh:7d2e346993455432397ebb990df250e7d9b17a5f4a50816dd2d7c75c36801dd6",
    "zh:7ebcd477c2b2f584333157aeb1069a0f716085369cd6e6ec27743a40bf07a556",
    "zh:8c044f75d8c5d782d8bc99dc98ad51073bebefc01c634a62445b6979ed74ed30",
    "zh:cc7c45e16a1de4781995b5ed5bd5e809e93a9eb72b8a5eee36af7d69c0425ae9",
    "zh:d61be2db1a189106c5b938164e048a4511d53f7d48ce7e085315990f0513de0e",
    "zh:e7ffc57a3bea1cf52ebc089ede569c2ee3442b28ba42dd1ae239b3daeb557fc2",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}

provider "registry.opentofu.org/hashicorp/time" {
  version = "0.12.1"
  hashes = [
    "h1:PnOB6IAQJoYi/r3iUH7Hml2c2zFrIzHksQsrK3VPjSI=",
    "zh:50a9b67d5f5f42adbdb7712f67858aa64b5670070f6710751239b535fb48a4df",
    "zh:5a846fae035e363aed75b966d64a56f3489a38083e8407aaa656730437f53ed7",
    "zh:6767f1fc8a679b48eaa4cd114da0d8185fb3546375f3a0fb3728f10fa3dbc551",
    "zh:85d3da407c828bf057cbc0e86c75ef3d0f9f74a73c4ea1b4aef18e33f41092b1",
    "zh:9180721325139431112c638f5382a740ff219782f81d6346cdff5bccc418a43f",
    "zh:9ba9989f905a64db1409a9a57649549c89c7aedfb55ae399a7fa9411aafaadac",
    "zh:b3d9e7afb6a742e9be0541bc434b00d849fdfab0b4b859ceb0296c26c541af15",
    "zh:c87da712d718acd9dd03f544b020c320699cb29df197be4f74783e3c3d80fc17",
    "zh:cb1abe07638ef6d7b41d0e86dfb12d60a513aca3395a5da7191947f7459821dd",
    "zh:ecff2e823ef49eda03663fa8ee8bdc17d27cd419dbdacbf1719f38812dbf417e",
  ]
}
